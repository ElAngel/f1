/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package formula1;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;

/**
 *
 * @author Raúl
 */

//Clase que gestiona el programa Fórmula 1
public final class VentanaF1 extends JFrame
{
    //Creamos los componentes
    Container panel;//Contenedor principal
    JTabbedPane tabPanel;//Panel que contiene las distintas pestañas
    JPanel tab1, tab2,tab3;//Creamos 3 pestañas, una para cada sección
    
    //Creamos los botones para las distintas secciones
    //Botón escuderia
    JButton be1 = new JButton("");    
    JButton be2 = new JButton("");
    JButton be3 = new JButton("");
    JButton be4 = new JButton("");
    JButton be5 = new JButton("");
    JButton be6 = new JButton("");
    JButton be7 = new JButton("");
    JButton be8 = new JButton("");
    JButton be9 = new JButton("");
    JButton be10 = new JButton("");
    JButton be11 = new JButton("");
    JButton be12 = new JButton("");
    
    //Botón piloto
    JButton bp1 = new JButton("");
    JButton bp2 = new JButton("");
    JButton bp3 = new JButton("");
    JButton bp4 = new JButton("");
    JButton bp5 = new JButton("");
    JButton bp6 = new JButton("");
    JButton bp7 = new JButton("");
    JButton bp8 = new JButton("");
    JButton bp9 = new JButton("");
    JButton bp10 = new JButton("");
    JButton bp11 = new JButton("");
    JButton bp12 = new JButton("");
    JButton bp13 = new JButton("");
    JButton bp14 = new JButton("");
    JButton bp15 = new JButton("");
    JButton bp16 = new JButton("");
    JButton bp17 = new JButton("");
    JButton bp18 = new JButton("");
    JButton bp19 = new JButton("");
    JButton bp20 = new JButton("");
    JButton bp21 = new JButton("");
    JButton bp22 = new JButton("");    
    JButton bp23 = new JButton("");
    JButton bp24 = new JButton("");
    
    //Botón cicuito
    JButton bc1 = new JButton("");
    JButton bc2 = new JButton("");
    JButton bc3 = new JButton("");
    JButton bc4 = new JButton("");
    JButton bc5 = new JButton("");
    JButton bc6 = new JButton("");
    JButton bc7 = new JButton("");
    JButton bc8 = new JButton("");
    JButton bc9 = new JButton("");
    JButton bc10 = new JButton("");
    JButton bc11 = new JButton("");
    JButton bc12 = new JButton("");
    JButton bc13 = new JButton("");
    JButton bc14 = new JButton("");
    JButton bc15 = new JButton("");
    JButton bc16 = new JButton("");
    JButton bc17 = new JButton("");
    JButton bc18 = new JButton("");
    JButton bc19 = new JButton("");
    JButton bc20 = new JButton("");
    
    //Creamos las distintas imágenes para los botones
    //Imágenes para las escuderias
    ImageIcon ferrari = new ImageIcon(".//img/Coches/ferrari.jpg");
    ImageIcon mercedes = new ImageIcon(".//img/Coches/mercedes.jpg");
    ImageIcon mclaren = new ImageIcon(".//img/Coches/mclaren.jpg");
    ImageIcon rbr = new ImageIcon(".//img/Coches/rbr.jpg");
    ImageIcon williams = new ImageIcon(".//img/Coches/williams.jpg");
    ImageIcon lotus = new ImageIcon(".//img/Coches/lotus.jpg");
    ImageIcon sauber = new ImageIcon(".//img/Coches/sauber.jpg");
    ImageIcon toro = new ImageIcon(".//img/Coches/toro.jpg");
    ImageIcon force = new ImageIcon(".//img/Coches/force.jpg");
    ImageIcon caterham = new ImageIcon(".//img/Coches/caterham.jpg");
    ImageIcon marussia = new ImageIcon(".//img/Coches/marussia.jpg");
    ImageIcon hrt = new ImageIcon(".//img/Coches/hrt.jpg");
    
    //Imágenes para los pilotos
    ImageIcon alonso = new ImageIcon(".//img/Pilotos/alonso.jpg");
    ImageIcon kimi = new ImageIcon(".//img/Pilotos/kimi.jpg");
    ImageIcon hamilton = new ImageIcon(".//img/Pilotos/hamilton.jpg");
    ImageIcon rosberg = new ImageIcon(".//img/Pilotos/rosberg.jpg");
    ImageIcon button = new ImageIcon(".//img/Pilotos/button.jpg");
    ImageIcon magnussen = new ImageIcon(".//img/Pilotos/magnussen.jpg");
    ImageIcon massa = new ImageIcon(".//img/Pilotos/massa.jpg");
    ImageIcon bottas = new ImageIcon(".//img/Pilotos/bottas.jpg");
    ImageIcon grosjean = new ImageIcon(".//img/Pilotos/grosjean.jpg");
    ImageIcon maldonado = new ImageIcon(".//img/Pilotos/maldonado.jpg");
    ImageIcon perez = new ImageIcon(".//img/Pilotos/perez.jpg");
    ImageIcon hulkenberg = new ImageIcon(".//img/Pilotos/hulkenberg.jpg");
    ImageIcon gutierrez = new ImageIcon(".//img/Pilotos/gutierrez.jpg");
    ImageIcon sutil = new ImageIcon(".//img/Pilotos/sutil.jpg");
    ImageIcon vergne = new ImageIcon(".//img/Pilotos/vergne.jpg");
    ImageIcon kvyat = new ImageIcon(".//img/Pilotos/kvyat.jpg");
    ImageIcon bianchi = new ImageIcon(".//img/Pilotos/bianchi.jpg");
    ImageIcon chilton = new ImageIcon(".//img/Pilotos/chilton.jpg");
    ImageIcon ericsson = new ImageIcon(".//img/Pilotos/ericsson.jpg");
    ImageIcon kobayashi = new ImageIcon(".//img/Pilotos/kobayashi.jpg");
    ImageIcon ricciardo = new ImageIcon(".//img/Pilotos/ricciardo.jpg");
    ImageIcon vettel = new ImageIcon(".//img/Pilotos/vettel.jpg");  
    ImageIcon delarosa = new ImageIcon(".//img/Pilotos/delarosa.jpg");
    ImageIcon karthikeyan = new ImageIcon(".//img/Pilotos/karthikeyan.jpg");
    
    //Imágenes para los circuitos
    ImageIcon albertpark = new ImageIcon(".//img/Circuitos/albertpark.jpg");
    ImageIcon sepang = new ImageIcon(".//img/Circuitos/sepang.jpg");
    ImageIcon bahrein = new ImageIcon(".//img/Circuitos/bahrein.jpg");
    ImageIcon shanghai = new ImageIcon(".//img/Circuitos/shanghai.jpg");
    ImageIcon montmelo = new ImageIcon(".//img/Circuitos/montmelo.jpg");
    ImageIcon montecarlo = new ImageIcon(".//img/Circuitos/montecarlo.jpg");
    ImageIcon gillesvilleneuve = new ImageIcon(".//img/Circuitos/gillesvilleneuve.jpg");
    ImageIcon silverston = new ImageIcon(".//img/Circuitos/silverston.jpg");
    ImageIcon valencia = new ImageIcon(".//img/Circuitos/valencia.jpg");
    ImageIcon nurburgring = new ImageIcon(".//img/Circuitos/nurburgring.jpg");
    ImageIcon hockenheim = new ImageIcon(".//img/Circuitos/hockenheim.jpg");
    ImageIcon hungaroring = new ImageIcon(".//img/Circuitos/hungaroring.jpg");
    ImageIcon spa = new ImageIcon(".//img/Circuitos/spa.jpg");
    ImageIcon monza = new ImageIcon(".//img/Circuitos/monza.jpg");
    ImageIcon marinabay = new ImageIcon(".//img/Circuitos/marinabay.jpg");
    ImageIcon suzuka = new ImageIcon(".//img/Circuitos/suzuka.jpg");
    ImageIcon sochi = new ImageIcon(".//img/Circuitos/sochi.jpg");
    ImageIcon austin = new ImageIcon(".//img/Circuitos/austin.jpg");
    ImageIcon interlagos = new ImageIcon(".//img/Circuitos/interlagos.jpg");
    ImageIcon yasmarina = new ImageIcon(".//img/Circuitos/yasmarina.jpg");
    
    //Constructor de la ventana F1
    public VentanaF1()
    {
        //Llamamos a super para darle un nombre a la cabecera de la ventana
        super("Formula 1 2014");
        
        //Creamos los distintos componentes
        //Creamos la barra de menú
        JMenuBar barraMenu = new JMenuBar();
        
        //Creamos las opciones que vamos a tener en la barra de menú
        JMenu menuOpciones = new JMenu("Archivo");
        JMenu menuOpciones1 = new JMenu("Base de datos");
        JMenu menuOpciones2 = new JMenu("Acerca de");        
        
        //Creamos las opciones que vamos a tener dentro de cada menú de opciones
        JMenuItem listar1 = new JMenuItem("Editar");       
        JMenuItem listar2 = new JMenuItem("Salir");
        JMenuItem listar12 = new JMenuItem("Fórmula 1 V 1.0");
        
        //Creamos los menus de bases de datos
        JMenu listar3 = new JMenu("BD piloto");
        JMenu listar4 = new JMenu("BD escuderia");       
        JMenu listar5 = new JMenu("BD circuito");
        
        //Creamos los submenús de bases de datos
        JMenuItem listar6 = new JMenuItem("Consulta");
        JMenuItem listar7 = new JMenuItem("Edición");
        JMenuItem listar13 = new JMenuItem("Copia Seguridad");
        JMenuItem listar8 = new JMenuItem("Consulta");
        JMenuItem listar9 = new JMenuItem("Edición");
        JMenuItem listar14 = new JMenuItem("Copia Seguridad");
        JMenuItem listar10 = new JMenuItem("Consulta");
        JMenuItem listar11 = new JMenuItem("Edición");   
        JMenuItem listar15 = new JMenuItem("Copia Seguridad");
        
        //Iniciamos los componentes
        panel = this.getContentPane();     
        panel.setLayout(new FlowLayout());//Ordena de izq a dch los distintos elmentos del panel       
        
        //Damos formato a la ventana
        setSize(1250,620);//Establece el tamaño de la ventana       
        setDefaultCloseOperation(EXIT_ON_CLOSE);//Cierra el programa al pinchar en la x de cierre de ventana
        setResizable(false);//No permite maximizar ni cambiar tamaño de ventana
        setLocationRelativeTo(null);//Nos muestra la ventana en el centro de la pantalla(null)
        setVisible(true);//Hace que la ventana sea visible(true)          
        
        //Llamamos a las lenguetas para crearlas dentro del scroll
        lenguetaEscuderias();
        lenguetaPilotos();
        lenguetaCircuitos();        
        
        //Creamos el panel que contiene las pestañas
        tabPanel = new JTabbedPane();
        
        //Le damos nombre a las pestañas
        tabPanel.addTab("Escuderias", tab1);
        tabPanel.addTab("Pilotos", tab2);
        tabPanel.addTab("Circuitos", tab3);
        
        //Damos formato al panel contenedor de pestañas
        add(tabPanel, BorderLayout.CENTER );
        tabPanel.setVisible(true);
        
        // La barra de menús de este frame
        setJMenuBar(barraMenu);
        
        // Añadimos el menú a la barra
        barraMenu.add(menuOpciones); 
        barraMenu.add(menuOpciones1); 
        barraMenu.add(menuOpciones2); 
        
        //Añadimos los items al menú
        listar3.add(listar6);
        listar3.add(listar7);
        listar3.add(listar13);
        listar4.add(listar8); 
        listar4.add(listar9);
        listar4.add(listar14);
        listar5.add(listar10); 
        listar5.add(listar11); 
        listar5.add(listar15);
        
        // Añadimos los elementos al menú     
        menuOpciones.add(listar1);         
        menuOpciones.add(listar2);      
        menuOpciones2.add(listar12); 
        menuOpciones1.add(listar3);
        menuOpciones1.add(listar4); 
        menuOpciones1.add(listar5);        
        
        //Inserción de una línea separadora en el menú
        menuOpciones.add(new JSeparator()); 
        
        //Establecemos oyentes para las opciones elegibles:           
        listar1.addActionListener(new OyenteMenuE()); 
        listar2.addActionListener(new OyenteMenuS());
        listar12.addActionListener(new OyenteMenuV());  
        listar6.addActionListener(new OyenteMenuBDConsultaPiloto());  
        listar8.addActionListener(new OyenteMenuBDConsultaEscuderia());  
        listar10.addActionListener(new OyenteMenuBDConsultaCircuito()); 
        listar7.addActionListener(new OyenteMenuBDEditaPiloto());  
        listar9.addActionListener(new OyenteMenuBDEditaEscuderia());  
        listar11.addActionListener(new OyenteMenuBDEditaCircuito()); 
        listar13.addActionListener(new OyenteMenuBDCopiaPiloto());  
        listar14.addActionListener(new OyenteMenuBDCopiaEscuderia());  
        listar15.addActionListener(new OyenteMenuBDCopiaCircuito());  
    }    
    
    //Creamos la lenguta de las escuderias
    public void lenguetaEscuderias()
    {
        //Componentes y formato
        tab1 = new JPanel();
        tab1.setPreferredSize(new Dimension(1225,620));
        tab1.setLayout(null);
        
        //Etiquetas de las imágenes
        JLabel img1 = new JLabel("");
        JLabel img2 = new JLabel("");
        JLabel img3 = new JLabel("");
        JLabel img4 = new JLabel("");
        JLabel img5 = new JLabel("");
        JLabel img6 = new JLabel("");
        JLabel img7 = new JLabel("");
        JLabel img8 = new JLabel("");
        JLabel img9 = new JLabel("");
        JLabel img10 = new JLabel("");
        JLabel img11 = new JLabel("");
        JLabel img12 = new JLabel("");          
        
        //Añadimos las imágenes a las etiquetas
        img1.setIcon(ferrari);
        img1.setVisible(true);
        img2.setIcon(mercedes);
        img2.setVisible(true);
        img3.setIcon(mclaren);
        img3.setVisible(true);
        img4.setIcon(rbr);
        img4.setVisible(true);
        img5.setIcon(williams);
        img5.setVisible(true);
        img6.setIcon(lotus);
        img6.setVisible(true);
        img7.setIcon(sauber);
        img7.setVisible(true);
        img8.setIcon(toro);
        img8.setVisible(true);
        img9.setIcon(force);
        img9.setVisible(true);
        img10.setIcon(caterham);
        img10.setVisible(true);
        img11.setIcon(marussia);
        img11.setVisible(true);
        img12.setIcon(hrt);
        img12.setVisible(true);        
        
        //Añadimos los botones a la pestaña de escuderias
        tab1.add(be1);
        tab1.add(be2);
        tab1.add(be3);
        tab1.add(be4);
        tab1.add(be5);
        tab1.add(be6);
        tab1.add(be7);
        tab1.add(be8);
        tab1.add(be9);
        tab1.add(be10);
        tab1.add(be11); 
        tab1.add(be12);
        
        //Posicionamos los botones en la pestaña
        be1.setBounds(0, 0, 300, 175);
        be2.setBounds(305, 0, 300, 175);
        be3.setBounds(610, 0, 300, 175);
        be4.setBounds(915, 0, 300, 175);
        be5.setBounds(0, 180, 300, 175);
        be6.setBounds(305, 180, 300, 175);
        be7.setBounds(610, 180, 300, 175);
        be8.setBounds(915, 180, 300, 175);
        be9.setBounds(0, 360, 300, 175);
        be10.setBounds(305, 360, 300, 175);
        be11.setBounds(610, 360, 300, 175);
        be12.setBounds(915, 360, 300, 175);        
        
        //Añadimos las imágenes a los botones
        be1.add(img1);
        be2.add(img2);
        be3.add(img3);
        be4.add(img4);
        be5.add(img5);
        be6.add(img6);
        be7.add(img7);
        be8.add(img8);
        be9.add(img9);
        be10.add(img10);
        be11.add(img11);
        be12.add(img12);
        
        //Enlazamos los oyentes a cada botón
        be1.addActionListener(new OyenteEscuderia1());
        be2.addActionListener(new OyenteEscuderia2());
        be3.addActionListener(new OyenteEscuderia3());
        be4.addActionListener(new OyenteEscuderia4());
        be5.addActionListener(new OyenteEscuderia5());
        be6.addActionListener(new OyenteEscuderia6());
        be7.addActionListener(new OyenteEscuderia7());
        be8.addActionListener(new OyenteEscuderia8());
        be9.addActionListener(new OyenteEscuderia9());
        be10.addActionListener(new OyenteEscuderia10());
        be11.addActionListener(new OyenteEscuderia11());
        be12.addActionListener(new OyenteEscuderia12());    
    }
    
    //Creamos la lenguta de los pilotos
    public void lenguetaPilotos()
    {
        //Componentes y formato
        tab2 = new JPanel();
        tab2.setPreferredSize(new Dimension(1225,620));
        tab2.setLayout(null);        
         
        //Etiquetas de las imágenes
        JLabel img1 = new JLabel("");
        JLabel img2 = new JLabel("");
        JLabel img3 = new JLabel("");
        JLabel img4 = new JLabel("");
        JLabel img5 = new JLabel("");
        JLabel img6 = new JLabel("");
        JLabel img7 = new JLabel("");
        JLabel img8 = new JLabel("");
        JLabel img9 = new JLabel("");
        JLabel img10 = new JLabel("");
        JLabel img11 = new JLabel("");
        JLabel img12 = new JLabel("");
        JLabel img13 = new JLabel("");
        JLabel img14 = new JLabel("");
        JLabel img15 = new JLabel("");
        JLabel img16 = new JLabel("");
        JLabel img17 = new JLabel("");
        JLabel img18 = new JLabel("");
        JLabel img19 = new JLabel("");
        JLabel img20 = new JLabel("");
        JLabel img21 = new JLabel("");
        JLabel img22 = new JLabel("");  
        JLabel img23 = new JLabel("");
        JLabel img24 = new JLabel("");
        
        //Añadimos las imágenes a las etiquetas
        img1.setIcon(alonso);
        img1.setVisible(true);        
        img2.setIcon(kimi);
        img2.setVisible(true);
        img3.setIcon(hamilton);
        img3.setVisible(true);
        img4.setIcon(rosberg);
        img4.setVisible(true);
        img5.setIcon(button);
        img5.setVisible(true);
        img6.setIcon(magnussen);
        img6.setVisible(true);
        img7.setIcon(massa);
        img7.setVisible(true);
        img8.setIcon(bottas);
        img8.setVisible(true);
        img9.setIcon(grosjean);
        img9.setVisible(true);
        img10.setIcon(maldonado);
        img10.setVisible(true);
        img11.setIcon(perez);
        img11.setVisible(true);
        img12.setIcon(hulkenberg);
        img12.setVisible(true);
        img13.setIcon(gutierrez);
        img13.setVisible(true);
        img14.setIcon(sutil);
        img14.setVisible(true);
        img15.setIcon(vergne);
        img15.setVisible(true);
        img16.setIcon(kvyat);
        img16.setVisible(true);
        img17.setIcon(bianchi);
        img17.setVisible(true);
        img18.setIcon(chilton);
        img18.setVisible(true);
        img19.setIcon(ericsson);
        img19.setVisible(true);
        img20.setIcon(kobayashi);
        img20.setVisible(true);
        img21.setIcon(ricciardo);
        img21.setVisible(true);
        img22.setIcon(vettel);
        img22.setVisible(true);
        img23.setIcon(delarosa);
        img23.setVisible(true);
        img24.setIcon(karthikeyan);
        img24.setVisible(true);
        
        //Añadimos los botones a la pestaña de pilotos
        tab2.add(bp1);        
        tab2.add(bp2);
        tab2.add(bp3);
        tab2.add(bp4);
        tab2.add(bp5);
        tab2.add(bp6);
        tab2.add(bp7);
        tab2.add(bp8);
        tab2.add(bp9);
        tab2.add(bp10);
        tab2.add(bp11);  
        tab2.add(bp12);
        tab2.add(bp13);
        tab2.add(bp14);
        tab2.add(bp15);
        tab2.add(bp16);
        tab2.add(bp17);
        tab2.add(bp18);
        tab2.add(bp19);
        tab2.add(bp20);
        tab2.add(bp21);
        tab2.add(bp22);
        tab2.add(bp23);
        tab2.add(bp24);
        
        //Posicionamos los botones en la pestaña
        bp1.setBounds(0, 25, 150, 150);
        bp2.setBounds(155, 25, 150, 150);
        bp3.setBounds(310, 25, 150, 150);
        bp4.setBounds(465, 25, 150, 150);
        bp5.setBounds(620, 25, 150, 150);
        bp6.setBounds(775, 25, 150, 150);
        bp7.setBounds(930, 25, 150, 150);
        bp8.setBounds(1085, 25, 150, 150);
        bp9.setBounds(0, 180, 150, 150);
        bp10.setBounds(155, 180, 150, 150);
        bp11.setBounds(310, 180, 150, 150);
        bp12.setBounds(465, 180, 150, 150);
        bp13.setBounds(620, 180, 150, 150);
        bp14.setBounds(775, 180, 150, 150);
        bp15.setBounds(930, 180, 150, 150);
        bp16.setBounds(1085, 180, 150, 150);
        bp17.setBounds(0, 335, 150, 150);
        bp18.setBounds(155, 335, 150, 150);
        bp19.setBounds(310, 335, 150, 150);
        bp20.setBounds(465, 335, 150, 150);
        bp21.setBounds(620, 335, 150, 150);
        bp22.setBounds(775, 335, 150, 150);
        bp23.setBounds(930, 335, 150, 150);
        bp24.setBounds(1085, 335, 150, 150);        
        
        //Añadimos las imágenes a los botones
        bp1.add(img1);
        bp2.add(img2);
        bp3.add(img3);
        bp4.add(img4);
        bp5.add(img5);
        bp6.add(img6);
        bp7.add(img7);
        bp8.add(img8);
        bp9.add(img9);
        bp10.add(img10);
        bp11.add(img11);
        bp12.add(img12);
        bp13.add(img13);
        bp14.add(img14);
        bp15.add(img15);
        bp16.add(img16);
        bp17.add(img17);
        bp18.add(img18);
        bp19.add(img19);
        bp20.add(img20);
        bp21.add(img21);
        bp22.add(img22);
        bp23.add(img23);
        bp24.add(img24);
        
        //Enlazamos los oyentes a cada botón
        bp1.addActionListener(new OyentePiloto1());        
        bp2.addActionListener(new OyentePiloto2());        
        bp3.addActionListener(new OyentePiloto3());        
        bp4.addActionListener(new OyentePiloto4());        
        bp5.addActionListener(new OyentePiloto5());        
        bp6.addActionListener(new OyentePiloto6());        
        bp7.addActionListener(new OyentePiloto7());        
        bp8.addActionListener(new OyentePiloto8());        
        bp9.addActionListener(new OyentePiloto9());        
        bp10.addActionListener(new OyentePiloto10());            
        bp11.addActionListener(new OyentePiloto11());        
        bp12.addActionListener(new OyentePiloto12());        
        bp13.addActionListener(new OyentePiloto13());        
        bp14.addActionListener(new OyentePiloto14());        
        bp15.addActionListener(new OyentePiloto15());        
        bp16.addActionListener(new OyentePiloto16());        
        bp17.addActionListener(new OyentePiloto17());        
        bp18.addActionListener(new OyentePiloto18());        
        bp19.addActionListener(new OyentePiloto19());        
        bp20.addActionListener(new OyentePiloto20());        
        bp21.addActionListener(new OyentePiloto21());         
        bp22.addActionListener(new OyentePiloto22());
        bp23.addActionListener(new OyentePiloto23());         
        bp24.addActionListener(new OyentePiloto24());       
    }
    
    //Creamos la lenguta de los circuitos
    public void lenguetaCircuitos()
    {
        //Componentes y formato
        tab3 = new JPanel();
        tab3.setPreferredSize(new Dimension(1225,620));
        tab3.setLayout(null);
        
        //Etiquetas de las imagenes
        JLabel img1 = new JLabel("");
        JLabel img2 = new JLabel("");
        JLabel img3 = new JLabel("");
        JLabel img4 = new JLabel("");
        JLabel img5 = new JLabel("");
        JLabel img6 = new JLabel("");
        JLabel img7 = new JLabel("");
        JLabel img8 = new JLabel("");
        JLabel img9 = new JLabel("");
        JLabel img10 = new JLabel("");
        JLabel img11 = new JLabel("");
        JLabel img12 = new JLabel("");
        JLabel img13 = new JLabel("");
        JLabel img14 = new JLabel("");
        JLabel img15 = new JLabel("");
        JLabel img16 = new JLabel("");
        JLabel img17 = new JLabel("");
        JLabel img18 = new JLabel("");
        JLabel img19 = new JLabel("");
        JLabel img20 = new JLabel("");
        
        //Añadimos las imágenes a las etiquetas
        img1.setIcon(albertpark);
        img1.setVisible(true);        
        img2.setIcon(sepang);
        img2.setVisible(true);
        img3.setIcon(bahrein);
        img3.setVisible(true);
        img4.setIcon(shanghai);
        img4.setVisible(true);
        img5.setIcon(montmelo);
        img5.setVisible(true);
        img6.setIcon(montecarlo);
        img6.setVisible(true);
        img7.setIcon(gillesvilleneuve);
        img7.setVisible(true);
        img8.setIcon(silverston);
        img8.setVisible(true);
        img9.setIcon(valencia);
        img9.setVisible(true);
        img10.setIcon(nurburgring);
        img10.setVisible(true);
        img11.setIcon(hockenheim);
        img11.setVisible(true);
        img12.setIcon(hungaroring);
        img12.setVisible(true);
        img13.setIcon(spa);
        img13.setVisible(true);
        img14.setIcon(monza);
        img14.setVisible(true);
        img15.setIcon(marinabay);
        img15.setVisible(true);
        img16.setIcon(suzuka);
        img16.setVisible(true);
        img17.setIcon(sochi);
        img17.setVisible(true);
        img18.setIcon(austin);
        img18.setVisible(true);
        img19.setIcon(interlagos);
        img19.setVisible(true);
        img20.setIcon(yasmarina);
        img20.setVisible(true);
        
        //Añadimos los botones a la pestaña de circuitos
        tab3.add(bc1);        
        tab3.add(bc2);
        tab3.add(bc3);
        tab3.add(bc4);
        tab3.add(bc5);
        tab3.add(bc6);
        tab3.add(bc7);
        tab3.add(bc8);
        tab3.add(bc9);
        tab3.add(bc10);
        tab3.add(bc11);  
        tab3.add(bc12);
        tab3.add(bc13);
        tab3.add(bc14);
        tab3.add(bc15);
        tab3.add(bc16);
        tab3.add(bc17);
        tab3.add(bc18);
        tab3.add(bc19);
        tab3.add(bc20);       
        
        //Posicionamos los botones en la pestaña
        bc1.setBounds(0, 0, 245, 125);
        bc2.setBounds(247, 0, 245, 125);
        bc3.setBounds(494, 0, 245, 125);
        bc4.setBounds(742, 0, 245, 125);
        bc5.setBounds(989, 0, 245, 125);
        bc6.setBounds(0, 127, 245, 125);
        bc7.setBounds(247, 127, 245, 125);
        bc8.setBounds(494, 127, 245, 125);
        bc9.setBounds(742, 127, 245, 125);
        bc10.setBounds(989, 127, 245, 125);
        bc11.setBounds(0, 254, 245, 125);
        bc12.setBounds(247, 254, 245, 125);
        bc13.setBounds(494, 254, 245, 125);
        bc14.setBounds(742, 254, 245, 125);
        bc15.setBounds(989, 254, 245, 125);
        bc16.setBounds(0, 381, 245, 125);
        bc17.setBounds(247, 381, 245, 125);
        bc18.setBounds(494, 381, 245, 125);
        bc19.setBounds(742, 381, 245, 125);
        bc20.setBounds(989, 381, 245, 125);
        
        //Añadimos las imágenes a los botones
        bc1.add(img1);
        bc2.add(img2);
        bc3.add(img3);
        bc4.add(img4);
        bc5.add(img5);
        bc6.add(img6);
        bc7.add(img7);
        bc8.add(img8);
        bc9.add(img9);
        bc10.add(img10);
        bc11.add(img11);
        bc12.add(img12);
        bc13.add(img13);
        bc14.add(img14);
        bc15.add(img15);
        bc16.add(img16);
        bc17.add(img17);
        bc18.add(img18);
        bc19.add(img19);
        bc20.add(img20);
        
        //Enlazamos los oyentes a cada botón
        bc1.addActionListener(new OyenteCircuito1());        
        bc2.addActionListener(new OyenteCircuito2());        
        bc3.addActionListener(new OyenteCircuito3());        
        bc4.addActionListener(new OyenteCircuito4());        
        bc5.addActionListener(new OyenteCircuito5());        
        bc6.addActionListener(new OyenteCircuito6());        
        bc7.addActionListener(new OyenteCircuito7());        
        bc8.addActionListener(new OyenteCircuito8());        
        bc9.addActionListener(new OyenteCircuito9());        
        bc10.addActionListener(new OyenteCircuito10());            
        bc11.addActionListener(new OyenteCircuito11());        
        bc12.addActionListener(new OyenteCircuito12());        
        bc13.addActionListener(new OyenteCircuito13());        
        bc14.addActionListener(new OyenteCircuito14());        
        bc15.addActionListener(new OyenteCircuito15());        
        bc16.addActionListener(new OyenteCircuito16());        
        bc17.addActionListener(new OyenteCircuito17());        
        bc18.addActionListener(new OyenteCircuito18());        
        bc19.addActionListener(new OyenteCircuito19());        
        bc20.addActionListener(new OyenteCircuito20());             
    }   
    
    //Oyente del menú editar
    class OyenteMenuE implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            //Llamamos a la clase editor
            new Editor().setVisible(true);
        }
    }
    
    //Oyente del menú salir
    class OyenteMenuS implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            //Llamamos al método que nos saca del programa
            System.exit(0);
        }
    }
    
    //Oyente del menú Acerca de
    class OyenteMenuV implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            //Muestra información sobre el programador
            JOptionPane.showMessageDialog(null, "Versión 1.0 creada por: "+"\n\r"
                    + "Raúl Márquez");
        }
    }
    
    //Oyente del menu consulta piloto en base de datos
    class OyenteMenuBDConsultaPiloto implements ActionListener
    {        
        @Override
        public void actionPerformed(ActionEvent e)
        {
            //Creamos el objeto bd y llamamos a la consulta de pilotos
            BaseDatos bd = new BaseDatos();
            bd.llamadaConsultaPiloto();
        }
    }    
    
    //Oyente del menu consulta escuderia en base de datos    
    class OyenteMenuBDConsultaEscuderia implements ActionListener
    {        
        @Override
        public void actionPerformed(ActionEvent e)
        {  
            //Creamos el objeto bd y llamamos a la consulta de escuderías
            BaseDatos bd = new BaseDatos();
            bd.llamadaConsultaEscuderia();
        }            
    }
    
    //Oyente del menu consulta circuito en base de datos
    class OyenteMenuBDConsultaCircuito implements ActionListener
    {        
        @Override
        public void actionPerformed(ActionEvent e)
        {  
            //Creamos el objeto bd y llamamos a la consulta de circuitos
            BaseDatos bd = new BaseDatos();
            bd.llamadaConsultaCircuito();
        }               
    }
    
    //Oyente del menu edita piloto en base de datos
    class OyenteMenuBDEditaPiloto implements ActionListener
    {       
        @Override
        public void actionPerformed(ActionEvent e)
        {
            //Creamos el objeto id y llamamos a la edición de pilotos
            TablasDatos id = new TablasDatos();
            id.tablaPiloto();           
        }
    }
    
    //Oyente del menu edita escuderia en base de datos
    class OyenteMenuBDEditaEscuderia implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            //Creamos el objeto id y llamamos a la edición de escuderías
           TablasDatos id = new TablasDatos();
           id.tablaEscuderia();
        }
    }
    
    //Oyente del menu edita circuito en base de datos
    class OyenteMenuBDEditaCircuito implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            //Creamos el objeto id y llamamos a la edición de circuitos
            TablasDatos id = new TablasDatos();
            id.tablaCircuito();
        }
    }
    
    //Oyente del menu edita circuito en base de datos
    class OyenteMenuBDCopiaPiloto implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            //Creamos el objeto id y llamamos a la edición de circuitos
            BaseDatos bd = new BaseDatos();
            bd.copiaSeguridadPiloto();
        }
    }
    
    //Oyente del menu edita circuito en base de datos
    class OyenteMenuBDCopiaEscuderia implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            //Creamos el objeto id y llamamos a la edición de circuitos
           BaseDatos bd = new BaseDatos();
            bd.copiaSeguridadEscuderia();
        }
    }
    
    //Oyente del menu edita circuito en base de datos
    class OyenteMenuBDCopiaCircuito implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            //Creamos el objeto id y llamamos a la edición de circuitos
            BaseDatos bd = new BaseDatos();
            bd.copiaSeguridadCircuito();
        }
    }
    
    //Oyentes de las escuderias(12)
    class OyenteEscuderia1 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Escuderia es = new Escuderia();
            es.ferrari();
        }
    }
    
    class OyenteEscuderia2 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Escuderia es = new Escuderia();
            es.mercedes();
        }
    }
    
    class OyenteEscuderia3 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Escuderia es = new Escuderia();
            es.mclaren();
        }
    }
    
    class OyenteEscuderia4 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Escuderia es = new Escuderia();
            es.rbr();
        }
    }
    
    class OyenteEscuderia5 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Escuderia es = new Escuderia();
            es.williams();
        }
    }
    
    class OyenteEscuderia6 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Escuderia es = new Escuderia();
            es.lotus();
        }
    }
    
    class OyenteEscuderia7 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Escuderia es = new Escuderia();
            es.sauber();
        }
    }
    
    class OyenteEscuderia8 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Escuderia es = new Escuderia();
            es.toro();
        }
    }
    
    class OyenteEscuderia9 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Escuderia es = new Escuderia();
            es.force();
        }
    }
    
    class OyenteEscuderia10 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Escuderia es = new Escuderia();
            es.caterham();
        }
    }
    
    class OyenteEscuderia11 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Escuderia es = new Escuderia();
            es.marussia();
        }
    }
    
    class OyenteEscuderia12 implements ActionListener
    {         
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Escuderia es = new Escuderia();
            es.hrt();
        }
    }
    
    //Oyentes de los pilotos(24)
    class OyentePiloto1 implements ActionListener
    {
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.fernando();            
        }
    }
    
    class OyentePiloto2 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.kimi();
        }
    }
    
    class OyentePiloto3 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.hamilton();
        }
    }
    
    class OyentePiloto4 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.rosberg();
        }
    }
    
    class OyentePiloto5 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.button();
        }
    }
    
    class OyentePiloto6 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.magnussen();
        }
    }
    
    class OyentePiloto7 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.massa();
        }
    }
    
    class OyentePiloto8 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.bottas();
        }
    }
    
    class OyentePiloto9 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.grosjean();
        }
    }
    
    class OyentePiloto10 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.maldonado();
        }
    }
    
    class OyentePiloto11 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.perez();
        }
    }
    
    class OyentePiloto12 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.hulkenberg();
        }
    }
    
    class OyentePiloto13 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.gutierrez();
        }
    }
    
    class OyentePiloto14 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.sutil();
        }
    }
    
    class OyentePiloto15 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.vergne();
        }
    }
    
    class OyentePiloto16 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.kvyat();
        }
    }
    
    class OyentePiloto17 implements ActionListener
    {       
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.bianchi();
        }
    }
    
    class OyentePiloto18 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.chilton();
        }
    }
    
    class OyentePiloto19 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.ericsson();
        }
    }
    
    class OyentePiloto20 implements ActionListener
    {       
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.kobayashi();
        }
    }
    
    class OyentePiloto21 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.ricciardo();
        }
    }
    
    class OyentePiloto22 implements ActionListener
    {       
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.vettel();
        }
    }
    
    class OyentePiloto23 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.deLaRosa();
        }
    }
    
    class OyentePiloto24 implements ActionListener
    {        
        @Override
        public void  actionPerformed(ActionEvent e)
        {
            Piloto p = new Piloto();
            p.karthikeyan();
        }
    }
    
    //Oyentes de los circuitos(20)
    class OyenteCircuito1 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.australia();
        }
    }
    
    class OyenteCircuito2 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.malasia();
        }
    }
    
    class OyenteCircuito3 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.bahrein();
        }
    }
    
    class OyenteCircuito4 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.china();
        }
    }
    
    class OyenteCircuito5 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.spain();
        }
    }
    
    class OyenteCircuito6 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.monaco();
        }
    }
    
    class OyenteCircuito7 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.canada();
        }
    }
    
    class OyenteCircuito8 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.reinoUnido();
        }
    }
    
    class OyenteCircuito9 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.valencia();
        }
    }
    
    class OyenteCircuito10 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.alemania();
        }
    }
    
    class OyenteCircuito11 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.hockenheim();
        }
    }
    
    class OyenteCircuito12 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.hungria();
        }
    }
    
    class OyenteCircuito13 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.belgica();
        }
    }
    
    class OyenteCircuito14 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.italia();
        }
    }
    
    class OyenteCircuito15 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.singapur();
        }
    }
    
    class OyenteCircuito16 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.japon();
        }
    }
    
    class OyenteCircuito17 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.rusia();
        }
    }
    
    class OyenteCircuito18 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.estadosUnidos();
        }
    }
    
    class OyenteCircuito19 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.brasil();
        }
    }
    
    class OyenteCircuito20 implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            Circuito c = new Circuito();
            c.abuDabi();
        }
    }
    
    //Main de la clase VentanaF1
    public static void main(String args[])
    {
        VentanaF1 pr = new VentanaF1();
    }
}